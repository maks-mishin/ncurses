Source: ncurses
Section: libs
Priority: required
Maintainer: Ncurses Maintainers <ncurses@packages.debian.org>
Uploaders: Sven Joachim <svenjoac@gmx.de>, Craig Small <csmall@debian.org>
Build-Depends: debhelper-compat (= 13),
               libgpm-dev [linux-any],
               pkgconf,
               autoconf-dickey (>= 2.52+20210509),
Build-Depends-Arch: g++-multilib [amd64 i386 powerpc ppc64 s390 sparc] <!nobiarch>
Build-Depends-Indep: ncurses-term <pkg.ncurses.terminfocheck>
Standards-Version: 4.6.2
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/debian/ncurses
Vcs-Git: https://salsa.debian.org/debian/ncurses.git
Homepage: https://invisible-island.net/ncurses/

Package: ncurses-base
Architecture: all
Section: misc
Essential: yes
Multi-Arch: foreign
Depends: ${misc:Depends}
Provides: ncurses-runtime
Replaces: ncurses-term (<< 6.4+20230603)
Breaks: neovim (<< 0.6.0), vim-common (<< 2:9.0.1000-2),
        ncurses-term (<< 6.4+20230625), cryptsetup-initramfs (<< 2:2.6.1)
Description: basic terminal type definitions
 The ncurses library routines are a terminal-independent method of
 updating character screens with reasonable optimization.
 .
 This package contains terminfo data files to support the most common types of
 terminal, including ansi, dumb, linux, rxvt, screen, sun, vt100, vt102, vt220,
 vt52, and xterm.

Package: ncurses-term
Architecture: all
Section: misc
Priority: standard
Multi-Arch: foreign
Depends: ncurses-base (= ${binary:Version}), ${misc:Depends}
Replaces: alacritty (<< 0.3.4~), jfbterm (<< 0.4.7-10),
          kon2 (<< 0.3.9b-21), libiterm1 (<< 0.5-9), tn5250 (<< 0.17.4-3),
          ncurses-base (<< 6.4+20230625), foot-terminfo (<< 1.15.1-2)
Breaks: vim-common (<< 2:9.0.1000-2), foot-terminfo (<< 1.15.1-2)
Provides: foot-terminfo (= ${binary:Version})
Description: additional terminal type definitions
 The ncurses library routines are a terminal-independent method of
 updating character screens with reasonable optimization.
 .
 This package contains all of the numerous terminal definitions not found in
 the ncurses-base package.

Package: libtinfo6
Architecture: any
Priority: optional
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Breaks: tmux (<< 3.3a-4)
Description: shared low-level terminfo library for terminal handling
 The ncurses library routines are a terminal-independent method of
 updating character screens with reasonable optimization.
 .
 This package contains the shared low-level terminfo library.

Package: libtinfo6-udeb
Package-Type: udeb
Section: debian-installer
Architecture: any
Priority: optional
Depends: ${shlibs:Depends}, ${misc:Depends}
Build-Profiles: <!noudeb>
Description: shared low-level terminfo library for terminal handling - udeb
 The ncurses library routines are a terminal-independent method of
 updating character screens with reasonable optimization.
 .
 This package contains the stripped-down udeb version of shared low-level
 terminfo library.

Package: libncurses6
Architecture: any
Priority: optional
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Depends: libtinfo6 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Recommends: libgpm2 [linux-any]
Breaks: cowdancer (<< 0.89~)
Description: shared libraries for terminal handling
 The ncurses library routines are a terminal-independent method of
 updating character screens with reasonable optimization.
 .
 This package contains the shared libraries necessary to run programs
 compiled with ncurses.

Package: libncurses-dev
Architecture: any
Section: libdevel
Priority: optional
Multi-Arch: same
Depends: libtinfo6 (= ${binary:Version}), libncurses6 (= ${binary:Version}),
         libncursesw6 (= ${binary:Version}), libc6-dev | libc-dev, ${misc:Depends}
Conflicts: ncurses-dev
Replaces: ncurses-dev
Provides: libncurses5-dev (= ${binary:Version}),
          libncursesw5-dev (= ${binary:Version}),
          libtinfo-dev (= ${binary:Version}), ncurses-dev
Suggests: ncurses-doc
Description: developer's libraries for ncurses
 The ncurses library routines are a terminal-independent method of
 updating character screens with reasonable optimization.
 .
 This package contains the header files, static libraries
 and symbolic links that developers using ncurses will need.

Package: libncursesw6
Architecture: any
Priority: optional
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Depends: libtinfo6 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Recommends: libgpm2 [linux-any]
Description: shared libraries for terminal handling (wide character support)
 The ncurses library routines are a terminal-independent method of
 updating character screens with reasonable optimization.
 .
 This package contains the shared libraries necessary to run programs
 compiled with ncursesw, which includes support for wide characters.

Package: libncursesw6-udeb
Package-Type: udeb
Section: debian-installer
Architecture: any
Priority: optional
Depends: libtinfo6-udeb (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Build-Profiles: <!noudeb>
Description: shared libraries for terminal handling (wide character support) - udeb
 The ncurses library routines are a terminal-independent method of
 updating character screens with reasonable optimization.
 .
 This package contains the stripped-down udeb version of shared libraries
 necessary to run programs compiled with ncursesw, which includes support
 for wide characters.

Package: lib64tinfo6
Architecture: i386 powerpc sparc s390
Priority: optional
Depends: ${shlibs:Depends}, ${misc:Depends}
Build-Profiles: <!nobiarch>
Description: shared low-level terminfo library for terminal handling (64-bit)
 The ncurses library routines are a terminal-independent method of
 updating character screens with reasonable optimization.
 .
 This package contains the shared low-level terminfo library.
 .
 This package supports the 64-bit ABI variant of your system's architecture.

Package: lib64ncurses6
Architecture: i386 powerpc sparc s390
Priority: optional
Depends: lib64tinfo6 (= ${binary:Version}),
         ${shlibs:Depends}, ${misc:Depends}
Build-Profiles: <!nobiarch>
Description: shared libraries for terminal handling (64-bit)
 The ncurses library routines are a terminal-independent method of
 updating character screens with reasonable optimization.
 .
 This package contains the shared libraries necessary to run programs
 compiled with ncurses.
 .
 This package supports the 64-bit ABI variant of your system's
 architecture.

Package: lib64ncursesw6
Architecture: i386 powerpc sparc s390
Priority: optional
Depends: lib64tinfo6 (= ${binary:Version}),
         ${shlibs:Depends}, ${misc:Depends}
Build-Profiles: <!nobiarch>
Description: shared libraries for terminal handling (wide character support) (64-bit)
 The ncurses library routines are a terminal-independent method of
 updating character screens with reasonable optimization.
 .
 This package contains the shared libraries necessary to run programs
 compiled with ncursesw, which includes support for wide characters.
 .
 This package supports the 64-bit ABI variant of your system's
 architecture.

Package: lib64ncurses-dev
Architecture: i386 powerpc sparc s390
Section: libdevel
Priority: optional
Depends: lib64ncurses6 (= ${binary:Version}),
	 lib64ncursesw6 (= ${binary:Version}),
         lib64tinfo6 (= ${binary:Version}),
         libncurses-dev (= ${binary:Version}), lib64c-dev, ${misc:Depends}
Suggests: ncurses-doc
Build-Profiles: <!nobiarch>
Description: developer's libraries for ncurses (64-bit)
 The ncurses library routines are a terminal-independent method of
 updating character screens with reasonable optimization.
 .
 This package contains the header files, static libraries
 and symbolic links that developers using ncurses will need.
 .
 This package supports the 64-bit ABI variant of your system's
 architecture.

Package: lib32tinfo6
Architecture: amd64 ppc64
Priority: optional
Depends: ${shlibs:Depends}, ${misc:Depends}
Build-Profiles: <!nobiarch>
Description: shared low-level terminfo library for terminal handling (32-bit)
 The ncurses library routines are a terminal-independent method of
 updating character screens with reasonable optimization.
 .
 This package contains the shared low-level terminfo library.
 .
 This package supports the 32-bit ABI variant of your system's architecture.

Package: lib32ncurses6
Architecture: amd64 ppc64
Priority: optional
Depends: lib32tinfo6 (= ${binary:Version}),
         ${shlibs:Depends}, ${misc:Depends}
Build-Profiles: <!nobiarch>
Description: shared libraries for terminal handling (32-bit)
 The ncurses library routines are a terminal-independent method of
 updating character screens with reasonable optimization.
 .
 This package contains the shared libraries necessary to run programs
 compiled with ncurses.
 .
 This package supports the 32-bit ABI variant of your system's
 architecture.

Package: lib32ncursesw6
Architecture: amd64 ppc64
Priority: optional
Depends: lib32tinfo6 (= ${binary:Version}),
         ${shlibs:Depends}, ${misc:Depends}
Build-Profiles: <!nobiarch>
Description: shared libraries for terminal handling (wide character support) (32-bit)
 The ncurses library routines are a terminal-independent method of
 updating character screens with reasonable optimization.
 .
 This package contains the shared libraries necessary to run programs
 compiled with ncursesw, which includes support for wide characters.
 .
 This package supports the 32-bit ABI variant of your system's
 architecture.

Package: lib32ncurses-dev
Architecture: amd64 ppc64
Section: libdevel
Priority: optional
Depends: lib32ncurses6 (= ${binary:Version}),
	 lib32ncursesw6 (= ${binary:Version}),
         lib32tinfo6 (= ${binary:Version}),
         libncurses-dev (= ${binary:Version}), lib32c-dev, ${misc:Depends}
Suggests: ncurses-doc
Build-Profiles: <!nobiarch>
Description: developer's libraries for ncurses (32-bit)
 The ncurses library routines are a terminal-independent method of
 updating character screens with reasonable optimization.
 .
 This package contains the header files, static libraries
 and symbolic links that developers using ncurses will need.
 .
 This package supports the 32-bit ABI variant of your system's
 architecture.

Package: ncurses-bin
Architecture: any
Section: utils
Essential: yes
Pre-Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: foreign
Description: terminal-related programs and man pages
 The ncurses library routines are a terminal-independent method of
 updating character screens with reasonable optimization.
 .
 This package contains the programs used for manipulating the terminfo
 database and individual terminfo entries, as well as some programs for
 resetting terminals and such.

Package: ncurses-examples
Architecture: any
Section: misc
Priority: optional
Depends: ${shlibs:Depends}, ${misc:Depends}
Build-Profiles: <!pkg.ncurses.noexamples>
Description: test programs and examples for ncurses
 The ncurses library routines are a terminal-independent method of
 updating character screens with reasonable optimization.
 .
 This package contains programs demonstrating the possibilities of
 ncurses and testing the library. The examples include an
 analog/digital clock and several classic programs such as solitaire,
 battleships, a knight's tour on a chess board, the towers of Hanoi
 and several others.

Package: ncurses-doc
Architecture: all
Section: doc
Priority: optional
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: developer's guide and documentation for ncurses
 The ncurses library routines are a terminal-independent method of
 updating character screens with reasonable optimization.
 .
 This package contains an introduction to writing programs with
 ncurses a guide to the internals of the ncurses library.  It also
 includes the libraries' man pages.
 .
 Non-developers likely have little use for this package.
